SpreadsheetResponse for Nette
=============================

Spreadsheet file response for apps based on [Nette Framework](https://nette.org) (resp. `nette/application` package).


Usage
-----
Inside the presenter render method:
```php
$spreadsheet = new PhpOffice\PhpSpreadsheet\Spreadsheet();

$response = new ElektroPotkan\NetteResponsesSpreadsheet\SpreadsheetResponse($spreadsheet, 'filename.xlsx');
$this->sendResponse($response);
```


Author
------
Elektro-potkan <git@elektro-potkan.cz>


Info
----
### Versioning
This project uses [Semantic Versioning 2.0.0 (semver.org)](https://semver.org).

### Branching
This project uses slightly modified Git-Flow Workflow and Branching Model:
- https://www.atlassian.com/git/tutorials/comparing-workflows/gitflow-workflow
- https://nvie.com/posts/a-successful-git-branching-model/


License
-------
You may use this program under the terms of either the BSD Zero Clause License or the GNU General Public License (GPL) version 3 or later.

See file [LICENSE](LICENSE.md).
