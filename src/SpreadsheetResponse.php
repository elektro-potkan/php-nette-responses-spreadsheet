<?php

declare(strict_types=1);

namespace ElektroPotkan\NetteResponsesSpreadsheet;

use InvalidArgumentException;
use Nette;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;


/**
 * Response for PhpSpreadsheet
 */
class SpreadsheetResponse implements Nette\Application\Response {
	use Nette\SmartObject;
	
	
	/** @var Spreadsheet */
	private $spreadsheet;
	
	/** @var string */
	private $spreadsheetWriterType = 'Xlsx';
	
	/** @var string */
	private $contentType = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet';
	
	/** @var string */
	private $filename;
	
	/** @var bool */
	private $forceDownload;
	
	
	/**
	 * Constructor
	 */
	public function __construct(Spreadsheet $spreadsheet, string $filename, bool $forceDownload = true){
		$this->spreadsheet = $spreadsheet;
		$this->filename = $filename;
		$this->forceDownload = $forceDownload;
	} // constructor
	
	/**
	 * Gets content type of HTTP response
	 */
	public function getContentType(): string {
		return $this->contentType;
	} // getContentType
	
	/**
	 * Sets content type of HTTP response
	 * @throws InvalidArgumentException
	 */
	public function setContentType(string $contentType): self {
		if(trim($contentType) === ''){
			throw new InvalidArgumentException('Content type must be non-empty string!');
		};
		
		$this->contentType = $contentType;
		return $this;
	} // setContentType
	
	/**
	 * Gets type of PhpSpreadsheet writer
	 */
	public function getWriterType(): string {
		return $this->spreadsheetWriterType;
	} // getWriterType
	
	/**
	 * Sets type of PhpSpreadsheet writer
	 */
	public function setWriterType(string $writerType): self {
		$this->spreadsheetWriterType = $writerType;
		return $this;
	} // setWriterType
	
	/**
	 * Sends response to output
	 */
	public function send(Nette\Http\IRequest $httpRequest, Nette\Http\IResponse $httpResponse): void {
		$httpResponse->setContentType($this->contentType);
		
		$httpResponse->setHeader(
			'Content-Disposition',
			($this->forceDownload ? 'attachment' : 'inline')
				. '; filename="' . $this->filename . '"'
				. '; filename*=utf-8\'\'' . rawurlencode($this->filename)
		);
		
		$writer = IOFactory::createWriter($this->spreadsheet, $this->spreadsheetWriterType);
		$writer->save('php://output');
	} // send
} // class SpreadsheetResponse
